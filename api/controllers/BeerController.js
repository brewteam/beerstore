/**
 * BeerController
 *
 * @description :: Server-side logic for managing Beers
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

    search: function(req, res) {
      Beer.find().where({name:{contains:req.param('name')}}).exec(function(err, beers){
        if(err) console.error(err);
        return res.json(beers);
      });
    },

    definition: function(req, res) {
        res.json(Beer.definition);
    }

};

