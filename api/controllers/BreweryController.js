/**
 * BreweryController
 *
 * @description :: Server-side logic for managing Breweries
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

  search: function(req, res) {
    Brewery.find().where({breweryName:{contains:req.param('name')}}).exec(function(err, breweries){
      if(err) console.error(err);
      return res.json(breweries);
    });
  },

  definition: function(req, res) {
    res.json(Brewery.definition);
  }

};

