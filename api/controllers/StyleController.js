/**
 * StyleController
 *
 * @description :: Server-side logic for managing Styles
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	
    
    definition: function(req, res) {
        res.json(Style.definition);
    }

};

