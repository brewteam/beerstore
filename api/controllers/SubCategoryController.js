/**
 * SubCategoryController
 *
 * @description :: Server-side logic for managing Subcategories
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	
    
    definition: function(req, res) {
        res.json(SubCategory.definition);
    }

};

