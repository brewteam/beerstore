/**
 * Beer.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

  attributes: {
    name:'string',
    style:'string',
    label:'string',
    country:'string',
    description:'string',
    school:'string',
    ebc:'string',
    glass:'string',
    temperature:'string',
    harmonization:'string',
    apresentation:'string',
    abv:'string',
    ean:'array',
    ibu:'string',
    brewery:{
      model:'Brewery'
    }
  }
};

