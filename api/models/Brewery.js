/**
 * Brewery.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

  attributes: {
    breweryName:'string',
    description:'string',
    country:'string',
    label:'string',
    url:'string',
    beers: {
      collection: 'Beer',
      via: 'brewery'
    }
  }
};

