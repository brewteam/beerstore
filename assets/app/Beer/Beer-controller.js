(function(ng, _) {

    'use strict';

    ng.module('beerstore')
        .controller('BeerCtrl', BeerCtrl)
        .controller('SingleBeerCtrl', SingleBeerCtrl);

    function BeerCtrl($scope,$rootScope, $state, Beers, BeerDefinition, SailsResourceService, $http) {
        var resourceService = new SailsResourceService('Beers'.toLowerCase());

        $scope.Beers = Beers;
        $scope.model_def = BeerDefinition.originalElement;
        $scope.Beer = {};

        $scope.remove = function remove(Beer) {
            Beer = Beer || $scope.Beer;
            if (window.confirm('Are you sure you want to delete this Beer?')) {
                return resourceService.remove(Beer, $scope.Beers);
            }
        };

        $scope.save = function save(Beer) {
          return resourceService.save(Beer, $scope.Beers)
            .then(function() {
              $state.go('^.list');
            }, function(err) {
              console.error('An error occured: ' + err);
            });
          Beer = Beer || $scope.Beer;
        };

      $scope.search = function(name){
        if (name.length >= 2) {
          $rootScope.blockLoading = true;
          $http.get('/Beers/search?name='+name)
            .success(function (data) {
              $scope.Beers = data;
            });
          $rootScope.blockLoading = null;
        }
      };

    }

    function SingleBeerCtrl($scope, $stateParams, Beers, BeerDefinition) {
        // coerce string -> int
        if (!_.isNaN($stateParams.id)) {
            $scope.Beer = _.find(Beers, {
                id: $stateParams.id
            });
        }
    }

})(
    window.angular,
    window._
);
