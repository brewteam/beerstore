(function(ng) {

    'use strict';

    ng.module('beerstore')
        .config(function($stateProvider, $urlRouterProvider) {

            $urlRouterProvider
                .when('/Beers', '/Beers/list');

            $stateProvider
                .state('Beers', {
                    abstract: true,
                    url: '/Beers',
                    controller: 'BeerCtrl',
                    template: '<div ui-view></div>',
                    resolve: {
                        BeerDefinition : function getBeerDefinition (SailsResourceDefinitions) {
                            return SailsResourceDefinitions.get('Beers');
                        },
                        Beers: function BeersListResolve(Restangular) {
                            return Restangular.all('Beers').getList();
                        }
                    }
                })
                .state('Beers.list', {
                    url: '/list',
                    templateUrl: 'app/Beer/Beer-list.html'
                })
                .state('Beers.add', {
                    url: '/add',
                    templateUrl: 'app/Beer/Beer-add-edit.html'
                })
                .state('Beers.info', {
                    url: '/info/:id',
                    controller: 'SingleBeerCtrl',
                    templateUrl: 'app/Beer/Beer-info.html'
                })
                .state('Beers.edit', {
                    url: '/edit/:id',
                    controller: 'SingleBeerCtrl',
                    templateUrl: 'app/Beer/Beer-add-edit.html'
                });
        });
})(
    window.angular
);
