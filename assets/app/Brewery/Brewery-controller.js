(function(ng, _) {

    'use strict';

    ng.module('beerstore')
        .controller('BreweryCtrl', BreweryCtrl)
        .controller('SingleBreweryCtrl', SingleBreweryCtrl);

    function BreweryCtrl($scope, $rootScope, $state, Breweries, BreweryDefinition, SailsResourceService, $http) {
        var resourceService = new SailsResourceService('Breweries'.toLowerCase());

        $scope.Breweries = Breweries;
        $scope.model_def = BreweryDefinition.originalElement;
        $scope.Brewery = {};

        $scope.remove = function remove(Brewery) {
            Brewery = Brewery || $scope.Brewery;
            if (window.confirm('Are you sure you want to delete this Brewery?')) {
                return resourceService.remove(Brewery, $scope.Breweries);
            }
        };

        $scope.save = function save(Brewery) {
            Brewery = Brewery || $scope.Brewery;
            return resourceService.save(Brewery, $scope.Breweries)
                .then(function() {
                    $state.go('^.list');
                }, function(err) {
                    console.error('An error occured: ' + err);
                });
        };

      $scope.search = function(name){
        if (name.length >= 2) {
          $rootScope.blockLoading = true;
          $http.get('/Breweries/search?name='+name)
            .success(function (data) {
              $scope.Breweries = data;
            });
          $rootScope.blockLoading = null;
        }
      };
    }

    function SingleBreweryCtrl($scope, $stateParams, Breweries, BreweryDefinition) {
        // coerce string -> int
        if (!_.isNaN($stateParams.id)) {
            $scope.Brewery = _.find(Breweries, {
                id: $stateParams.id
            });
        }
    }

})(
    window.angular,
    window._
);
