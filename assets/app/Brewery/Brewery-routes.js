(function(ng) {

    'use strict';

    ng.module('beerstore')
        .config(function($stateProvider, $urlRouterProvider) {

            $urlRouterProvider
                .when('/Breweries', '/Breweries/list');

            $stateProvider
                .state('Breweries', {
                    abstract: true,
                    url: '/Breweries',
                    controller: 'BreweryCtrl',
                    template: '<div ui-view></div>',
                    resolve: {
                        BreweryDefinition : function getBreweryDefinition (SailsResourceDefinitions) {
                            return SailsResourceDefinitions.get('Breweries');
                        },
                        Breweries: function BreweriesListResolve(Restangular) {
                            return Restangular.all('Breweries').getList();
                        }
                    }
                })
                .state('Breweries.list', {
                    url: '/list',
                    templateUrl: 'app/Brewery/Brewery-list.html'
                })
                .state('Breweries.add', {
                    url: '/add',
                    templateUrl: 'app/Brewery/Brewery-add-edit.html'
                })
                .state('Breweries.info', {
                    url: '/info/:id',
                    controller: 'SingleBreweryCtrl',
                    templateUrl: 'app/Brewery/Brewery-info.html'
                })
                .state('Breweries.edit', {
                    url: '/edit/:id',
                    controller: 'SingleBreweryCtrl',
                    templateUrl: 'app/Brewery/Brewery-add-edit.html'
                });
        });
})(
    window.angular
);
