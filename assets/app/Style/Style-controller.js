(function(ng, _) {

    'use strict';

    ng.module('beerstore')
        .controller('StyleCtrl', StyleCtrl)
        .controller('SingleStyleCtrl', SingleStyleCtrl);

    function StyleCtrl($scope, $state, Styles, StyleDefinition, SailsResourceService) {
        var resourceService = new SailsResourceService('Styles'.toLowerCase());
        
        $scope.Styles = Styles;
        $scope.model_def = StyleDefinition.originalElement;
        $scope.Style = {};

        $scope.remove = function remove(Style) {
            Style = Style || $scope.Style;
            if (window.confirm('Are you sure you want to delete this Style?')) {
                return resourceService.remove(Style, $scope.Styles);
            }
        };

        $scope.save = function save(Style) {
            Style = Style || $scope.Style;
            return resourceService.save(Style, $scope.Styles)
                .then(function() {
                    $state.go('^.list');
                }, function(err) {
                    console.error('An error occured: ' + err);
                });
        };
    }

    function SingleStyleCtrl($scope, $stateParams, Styles, StyleDefinition) {
        // coerce string -> int
        $stateParams.id = _.parseInt($stateParams.id);
        if (!_.isNaN($stateParams.id)) {
            $scope.Style = _.find(Styles, {
                id: $stateParams.id
            });
        }
    }

})(
    window.angular,
    window._
);
