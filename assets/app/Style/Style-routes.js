(function(ng) {
    
    'use strict';

    ng.module('beerstore')
        .config(function($stateProvider, $urlRouterProvider) {

            $urlRouterProvider
                .when('/Styles', '/Styles/list');

            $stateProvider
                .state('Styles', {
                    abstract: true,
                    url: '/Styles',
                    controller: 'StyleCtrl',
                    template: '<div ui-view></div>',
                    resolve: {
                        StyleDefinition : function getStyleDefinition (SailsResourceDefinitions) {
                            return SailsResourceDefinitions.get('Styles');
                        },
                        Styles: function StylesListResolve(Restangular) {
                            return Restangular.all('Styles').getList();
                        }
                    },
                })
                .state('Styles.list', {
                    url: '/list',
                    templateUrl: 'app/Style/Style-list.html'
                })
                .state('Styles.add', {
                    url: '/add',
                    templateUrl: 'app/Style/Style-add-edit.html'
                })
                .state('Styles.info', {
                    url: '/info/:id',
                    controller: 'SingleStyleCtrl',
                    templateUrl: 'app/Style/Style-info.html'
                })
                .state('Styles.edit', {
                    url: '/edit/:id',
                    controller: 'SingleStyleCtrl',
                    templateUrl: 'app/Style/Style-add-edit.html'
                });
        });
})(
    window.angular
);
