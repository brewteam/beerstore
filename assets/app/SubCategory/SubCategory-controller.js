(function(ng, _) {

    'use strict';

    ng.module('beerstore')
        .controller('SubCategoryCtrl', SubCategoryCtrl)
        .controller('SingleSubCategoryCtrl', SingleSubCategoryCtrl);

    function SubCategoryCtrl($scope, $state, Subcategories, SubCategoryDefinition, SailsResourceService) {
        var resourceService = new SailsResourceService('Subcategories'.toLowerCase());
        
        $scope.Subcategories = Subcategories;
        $scope.model_def = SubCategoryDefinition.originalElement;
        $scope.SubCategory = {};

        $scope.remove = function remove(SubCategory) {
            SubCategory = SubCategory || $scope.SubCategory;
            if (window.confirm('Are you sure you want to delete this SubCategory?')) {
                return resourceService.remove(SubCategory, $scope.Subcategories);
            }
        };

        $scope.save = function save(SubCategory) {
            SubCategory = SubCategory || $scope.SubCategory;
            return resourceService.save(SubCategory, $scope.Subcategories)
                .then(function() {
                    $state.go('^.list');
                }, function(err) {
                    console.error('An error occured: ' + err);
                });
        };
    }

    function SingleSubCategoryCtrl($scope, $stateParams, Subcategories, SubCategoryDefinition) {
        // coerce string -> int
        $stateParams.id = _.parseInt($stateParams.id);
        if (!_.isNaN($stateParams.id)) {
            $scope.SubCategory = _.find(Subcategories, {
                id: $stateParams.id
            });
        }
    }

})(
    window.angular,
    window._
);
