(function(ng) {
    
    'use strict';

    ng.module('beerstore')
        .config(function($stateProvider, $urlRouterProvider) {

            $urlRouterProvider
                .when('/Subcategories', '/Subcategories/list');

            $stateProvider
                .state('Subcategories', {
                    abstract: true,
                    url: '/Subcategories',
                    controller: 'SubCategoryCtrl',
                    template: '<div ui-view></div>',
                    resolve: {
                        SubCategoryDefinition : function getSubCategoryDefinition (SailsResourceDefinitions) {
                            return SailsResourceDefinitions.get('Subcategories');
                        },
                        Subcategories: function SubcategoriesListResolve(Restangular) {
                            return Restangular.all('Subcategories').getList();
                        }
                    },
                })
                .state('Subcategories.list', {
                    url: '/list',
                    templateUrl: 'app/SubCategory/SubCategory-list.html'
                })
                .state('Subcategories.add', {
                    url: '/add',
                    templateUrl: 'app/SubCategory/SubCategory-add-edit.html'
                })
                .state('Subcategories.info', {
                    url: '/info/:id',
                    controller: 'SingleSubCategoryCtrl',
                    templateUrl: 'app/SubCategory/SubCategory-info.html'
                })
                .state('Subcategories.edit', {
                    url: '/edit/:id',
                    controller: 'SingleSubCategoryCtrl',
                    templateUrl: 'app/SubCategory/SubCategory-add-edit.html'
                });
        });
})(
    window.angular
);
