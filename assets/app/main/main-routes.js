(function(ng) {
    'use strict';

    ng.module('beerstore')
        .config(function($stateProvider, $urlRouterProvider) {

            $urlRouterProvider
                .when('/', '/main');

            $stateProvider
                .state('main', {
                    url: '/main',

                    controller: function MainCtrl($scope) {
                        $scope.app = {
                            name: 'beerstore',
                            description: 'An Angular-frontend-based Sails application'
                        };
                    },
                    templateUrl: 'app/main/main.html'
                });
        });
})(
    window.angular
);
